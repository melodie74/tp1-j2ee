package Servlet;

import fonction.Operation;
import informations.Colis;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/CreerColis")
public class CreerColis extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @EJB
    private Operation oper;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreerColis() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String num = request.getParameter("numero");

        PrintWriter out = response.getWriter();
        if (num != null ) {
            Colis c = oper.creerColis(num);
            request.setAttribute("colis", c);
            request.getRequestDispatcher("/AfficherCompte.jsp").forward(request, response);
        } else {
            out.println("creation compte KO");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher("/creerColis.jsp").forward(request, response);



    }
}
