package informations;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Colis {

    private static final long serialVersionUID = 1L;

    @Id
    private int id;
    private float poid;
    private double valeur;
    private Emplacement origine;
    private Emplacement destination;
    private Etat etat;
    private Emplacement lieuActuel;

    public Colis(int id, float poid, double valeur, Emplacement origine, Emplacement destination) {
        this.id = id;
        this.poid = poid;
        this.valeur = valeur;
        this.origine = origine;
        this.destination = destination;
    }

    public Emplacement getLieuActuel() {
        return lieuActuel;
    }

    public void setLieuActuel(Emplacement lieuActuel) {
        this.lieuActuel = lieuActuel;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public Emplacement getDestination() {
        return destination;
    }

    public void setDestination(Emplacement destination) {
        this.destination = destination;
    }

    public Emplacement getOrigine() {
        return origine;
    }

    public void setOrigine(Emplacement origine) {
        this.origine = origine;
    }

    public double getValeur() {
        return valeur;
    }

    public void setValeur(double valeur) {
        this.valeur = valeur;
    }

    public float getPoid() {
        return poid;
    }

    public void setPoid(float poid) {
        this.poid = poid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
