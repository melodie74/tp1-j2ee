package informations;

import javax.persistence.Entity;

@Entity
public enum Etat {
    ENREGISTREMENT,EN_ATTENTE,EN_ACHEMINEMENT,BLOQUE,LIVRE
}