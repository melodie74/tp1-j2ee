package fonction;

import informations.Colis;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Remote
@Stateless
public class OperationBean implements Operation{

    @PersistenceContext
    private EntityManager em;

    public OperationBean() {
        super();
    }

    @Override
    public Colis creerColis(String num) {
        Colis cpt = em.find(Colis.class, num);
        return cpt;
    }

}
